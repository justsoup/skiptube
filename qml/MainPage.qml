import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    id: mainPage

    header: PageHeader {
        id: header
        title: i18n.tr("SkipTube")

        trailingActionBar.actions: [
            Action {
                iconName: 'info'
                text: i18n.tr("About")
                onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'))
            }
        ]
    }
}