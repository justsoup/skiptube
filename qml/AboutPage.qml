import QtQuick 2.7
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3

Page {
    header: PageHeader {
        id: header
        title: i18n.tr('About')
    }

    Flickable {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        contentHeight: contentColumn.height + units.gu(4)

        ColumnLayout {
            id: contentColumn
            anchors {
                left: parent.left;
                top: parent.top;
                right: parent.right;
                margins: units.gu(2)
            }

            spacing: units.gu(2)

            Label {
                Layout.fillWidth: true
                text: i18n.tr('SkipTube')
                textSize: Label.XLarge
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            Item {
                Layout.preferredHeight: logo.height
                Layout.fillWidth: true

                Image {
                    id: logo
                    //height: width
                    //width: Math.min(parent.width/2, parent.height/2)
                    source: '../assets/logo.svg'
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }

            Label {
                Layout.fillWidth: true
                text: i18n.tr('A free as in speech Ubuntu Touch streaming client.');
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
        }
    }
}
