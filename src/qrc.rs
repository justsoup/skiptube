qrc!(qml_resources,
    "/" {
        "qml/Main.qml",
        "qml/MainPage.qml",
        "qml/AboutPage.qml",
        "assets/logo.svg"
    },
);

pub fn load() {
    qml_resources();
}
